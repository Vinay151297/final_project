import sys
import heapq
from time import time
from copy import deepcopy

class Coordinates:

    def __init__(self, x_coordinate, y_coordinate):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate

    def __eq__(self, other):
        if self.x_coordinate == other.x_coordinate and self.y_coordinate == other.y_coordinate:
            return True
        else:
            return False

    def __add__(self, other):
        x_coordinate = self.x_coordinate + other.x_coordinate
        y_coordinate = self.y_coordinate + other.y_coordinate
        return Coordinates(x_coordinate, y_coordinate)

    def __hash__(self):
        return hash((self.x_coordinate, self.y_coordinate))

    def __str__(self):
        return str(self.x_coordinate) + ', ' + str(self.y_coordinate)

    def double(self):
        ''' Used for checking past the box to see what's behind it '''
        return Coordinates(self.x_coordinate * 2, self.y_coordinate * 2)

class Direction:

    def __init__(self, position, char):
        self.position = position
        self.char = char

    def __str__(self):
        return self.char

L = Direction(Coordinates(-1, 0), 'l')
R = Direction(Coordinates(1, 0), 'r')
U = Direction(Coordinates(0, -1), 'u')
D = Direction(Coordinates(0, 1), 'd')
directions = [U, D, L, R]

class MyQueue:

    def __init__(self):
        self.q = []

    def push(self, x):
        ''' inserts at the beginning of the list '''
        self.q.insert(0, x)

    def pop(self):
        ''' removes the first item in the list '''
        return self.q.pop(0)

    def isEmpty(self):
        ''' checks for empty list '''
        if len(self.q) == 0:
            return True

    def __len__(self):
        ''' overriding len() '''
        return len(self.q)

class Board:

    def __init__(self, dir_list):
        self.dir_list = dir_list  # list of directions for solution
        self.walls = set()
        self.goals = set()
        self.boxes = set()
        self.fboxes = frozenset()  # since set() is not hashable
        self.player = None
        self.cost = 1  # used for GbfS and heuristic searches

    def __eq__(self, other):
        ''' checking for 'equality' of box positions and player positions '''
        if self.boxes.issubset(other.boxes) and self.player == other.player:
            return True
        else:
            return False

    def __hash__(self):
        ''' hashes by frozenset of box positions '''
        return hash((self.fboxes, self.player))

    def __gt__(self, other):
        ''' comparison by cost '''
        if self.cost > other.cost:
            return True
        else:
            return False

    def __lt__(self, other):
        ''' comparison by cost '''
        if self.cost < other.cost:
            return True
        else:
            return False

    def add_wall(self, x_coordinate, y_coordinate):
        self.walls.add(Coordinates(x_coordinate, y_coordinate))

    def add_goal(self, x_coordinate, y_coordinate):
        self.goals.add(Coordinates(x_coordinate, y_coordinate))

    def add_box(self, x_coordinate, y_coordinate):
        self.boxes.add(Coordinates(x_coordinate, y_coordinate))

    def set_player(self, x_coordinate, y_coordinate):
        self.player = Coordinates(x_coordinate, y_coordinate)

    def moves_available(self):
        moves = []
        for d in directions:
            if self.player + d.position not in self.walls:
                if self.player + d.position in self.boxes:
                # what if there's a wall or box behind it?
                    if self.player + d.position.double() not in self.boxes.union(self.walls):
                        moves.append(d)
                else:
                    moves.append(d)
        return moves

    def move(self, direction):
        ''' moves player and box '''
        p = self.player + direction.position
        if p in self.boxes:
            self.boxes.remove(p)
            self.boxes.add(p + direction.position)
        self.player = p
        self.dir_list.append(direction)

    def is_win(self):
        ''' Checks for winning/final state '''
        if self.goals.issubset(self.boxes):
            return True
        else:
            return False

    def getDirections(self):
        ''' Outputs the list of directions taken for the solution '''
        chars = ''
        for d in self.dir_list:
            chars += d.char
            chars += ', '
        return chars

class Bfs:

    def __init__(self, board_1):
        self.board = board_1

    def print_results(self, board_1, nodes_gen, nodes_rep, fri_nodes, nodes_expl, dur):
        print ("\n1. Breadth-first search")
        print ("Solution: " + board_1.getDirections())
        print ("Nodes generated: " + str(nodes_gen))
        print ("Nodes repeated: " + str(nodes_rep))
        print ("Fringe nodes: " + str(fri_nodes))
        print ("Explored nodes: " + str(nodes_expl))
        print ('Duration: ' + str(dur) + ' secs')


    def search(self):
        board_1 = self.board
        start = time()
        nodes_generated = 0
        nodes_repeated = 0
        if board_1.is_win():
            end = time()
            self.print_results(board_1, 1, 0, 0, 1, end - start)
            return board_1
        node = deepcopy(board_1)
        nodes_generated += 1
        frontier = MyQueue()
        frontier.push(node)
        explored = set()
        keepLooking = True
        while keepLooking:
            if frontier.isEmpty():
                print ("Solution not found")
                return
            else:
                currNode = frontier.pop()
                moves = currNode.moves_available()
                currNode.fboxes = frozenset(currNode.boxes)
                explored.add(currNode)
                for m in moves:
                    child = deepcopy(currNode)
                    nodes_generated += 1
                    child.move(m)
                    if child not in explored:
                        if child.is_win():
                            end = time()
                            self.print_results(child, nodes_generated, nodes_repeated, len(
                                          frontier), len(explored), end - start)
                            return child
                        frontier.push(child)
                    else:
                        nodes_repeated += 1

class Dfs:

    def __init__(self, board_1):
        self.board = board_1

    def print_results(self, board_1, nodes_gen, nodes_rep, fri_nodes, nodes_expl, dur):
        print ("\n2. Depth-first search")
        print ("Solution: " + board_1.getDirections())
        print ("Nodes generated: " + str(nodes_gen))
        print ("Nodes repeated: " + str(nodes_rep))
        print ("Fringe nodes: " + str(fri_nodes))
        print ("Explored nodes: " + str(nodes_expl))
        print ('Duration: ' + str(dur) + ' secs')

    def search(self):
        board_1 = self.board
        start = time()
        nodes_generated = 0
        nodes_repeated = 0
        if board_1.is_win():
            end = time()
            self.print_results(board_1, 1, 0, 0, 1, end - start)
            return board_1
        node = deepcopy(board_1)
        nodes_generated += 1
        frontier = []
        frontierSet = set()
        heapq.heappush(frontier, node)
        frontierSet.add(node)
        explored = set()
        keepLooking = True
        while keepLooking:
            if len(frontier) == 0:
                print ("Solution not found")
                return
            else:
                currNode = heapq.heappop(frontier)
                frontierSet.remove(currNode)
                if currNode.is_win():
                    end = time()
                    print_results(currNode, nodes_generated, nodes_repeated, len(
                                  frontier), len(explored), end - start)
                    return currNode
                moves = currNode.moves_available()
                currNode.fboxes = frozenset(currNode.boxes)
                explored.add(currNode)
                for m in moves:
                    child = deepcopy(currNode)
                    nodes_generated += 1
                    child.move(m)
                    if child.is_win():
                        end = time()
                        self.print_results(child, nodes_generated, nodes_repeated, len(
                                      frontier), len(explored), end - start)
                        return child
                    if child not in explored:
                        if child not in frontierSet:
                            heapq.heappush(frontier, child)
                            frontierSet.add(child)
                    elif child in frontierSet:
                        count = frontier.count(child)
                        i = 0
                        while i <= count:
                            a = frontier.pop((frontier.index(child)))
                            if child.cost < a.cost:
                                heapq.heappush(frontier, child)
                                child = a
                                i = count + 1
                            else:
                                heapq.heappush(frontier, a)
                                i += 1
                        nodes_repeated += 1

class Gbfs:

    def __init__(self, board_1):
        self.board = board_1

    def print_results(self, board_1, nodes_gen, nodes_rep, fri_nodes, nodes_expl, dur):
        print ("\n4. Greedy best first search")
        print ("Solution: " + board_1.getDirections())
        print ("Nodes generated: " + str(nodes_gen))
        print ("Nodes repeated: " + str(nodes_rep))
        print ("Fringe nodes: " + str(fri_nodes))
        print ("Explored nodes: " + str(nodes_expl))
        print ('Duration: ' + str(dur) + ' secs')


    def search(self):
        board_1 = self.board
        start = time()
        nodes_generated = 0
        nodes_repeated = 0
        if board_1.is_win():
            end = time()
            self.print_results(board_1, 1, 0, 0, 1, end - start)
            return board_1
        node = deepcopy(board_1)
        nodes_generated += 1
        frontier = []
        frontierSet = set()
        heapq.heappush(frontier, node)
        frontierSet.add(node)
        explored = set()
        keepLooking = True
        while keepLooking:
            if len(frontier) == 0:
                print ("Solution not found")
                return
            else:
                currNode = heapq.heappop(frontier)
                frontierSet.remove(currNode)
                if currNode.is_win():
                    end = time()
                    self.print_results(currNode, nodes_generated, nodes_repeated, len(
                                  frontier), len(explored), end - start)
                    return currNode
                moves = currNode.moves_available()
                currNode.fboxes = frozenset(currNode.boxes)
                explored.add(currNode)
                for m in moves:
                    child = deepcopy(currNode)
                    nodes_generated += 1
                    child.move(m)
                    if child.is_win():
                        end = time()
                        self.print_results(child, nodes_generated, nodes_repeated, len(
                                      frontier), len(explored), end - start)
                        return child
                    if child not in explored:
                        if child not in frontierSet:
                            heapq.heappush(frontier, child)
                            frontierSet.add(child)
                    elif child in frontierSet:
                        count = frontier.count(child)
                        index = 0
                        while index <= count:
                            a = frontier.pop((frontier.index(child)))
                            if child.cost < a.cost:
                                heapq.heappush(frontier, child)
                                child = a
                                index = count + 1
                            else:
                                heapq.heappush(frontier, a)
                                index += 1
                        nodes_repeated += 1


class Sokoban:

    def new_board(self, filename):
        ''' Creates new board from file '''
        empty = []  # empty solution list
        board = Board(empty)
        with open(filename, 'r') as file:  # automatically closes file
            read_data = file.read()
            lines = read_data.split('\n')
            height = lines.pop(0)
            x_coordinate = 0
            y_coordinate = 0
            temp = 0
            for line in lines:
                for char in line:
                    # adds Coordinates to board's sets by reading in char
                    if char == '#':
                        board.add_wall(x_coordinate, y_coordinate)
                    elif char == '.':
                        board.add_goal(x_coordinate, y_coordinate)
                    elif char == '@':
                        board.set_player(x_coordinate, y_coordinate)
                        temp = 1
                    elif char == '+':
                        # player gets its own Coordinate marker
                        board.set_player(x_coordinate, y_coordinate)
                        board.add_goal(x_coordinate, y_coordinate)
                    elif char == '$':
                        board.add_box(x_coordinate, y_coordinate)
                    elif char == '*':
                        board.add_box(x_coordinate, y_coordinate)
                        board.add_goal(x_coordinate, y_coordinate)
                    x_coordinate += 1
                y_coordinate += 1
                x_coordinate = 0
            # check for a board with no player
            if temp == 1:
                print ("There is a player")
                return board
            else:
                print ("No player on Board")
                sys.exit()

    def doSearches(self, board_1, option):
        bfsObj = Bfs(board_1)
        dfsObj = Dfs(board_1)
        gbfsObj = Gbfs(board_1)
        if option == 1:
            bfsObj.search()
        if option == 2:
            dfsObj.search()
        if option == 3:
            gbfsObj.search()
        if option == 4:  # all get executed
            bfsObj.search()
            dfsObj.search()
            gbfsObj.search()

def runSearch(game, filename, option):
    ''' Runs the search based on filename and option selected '''
    board = game.new_board(filename)
    print ('\nSolving ' + filename + '...')
    game.doSearches(board, option)

sok = Sokoban()

print ("Which algorithm?")
print ("1) Breadth first search")
print ("2) Depth first search")
print ("3) Greedy best first search")
print ("4) all")
p = input("Type a number and press enter: ")
option = int(p)


if len(sys.argv) == 2:
    runSearch(sok, sys.argv[1], option)
else:
    runSearch(sok, 'level.txt', option)