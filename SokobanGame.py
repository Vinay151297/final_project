import pygame
import sys
import copy
import os
from pygame.locals import *

Cam_speed = 5

blue   =   0,  0,255    
white  = 255,255,255

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

Tile_width = 50
Tile_height = 45
sound = 0

def main():
	global Clock, Display_surface, Image_dictionary, Tile_mapping, Basic_font, Player
	pygame.init()
	Clock = pygame.time.Clock()

	Display_surface = pygame.display.set_mode((800,600))
	
	pygame.display.set_caption('SOKOBAN')
	Basic_font = pygame.font.Font('freesansbold.ttf',18)
	Image_dictionary = {
		'wall': pygame.image.load('Wall.png'),
		'box': pygame.image.load('Box.png'),
		'player': pygame.image.load('Player.png'),
		'goal': pygame.image.load('Goal.png'),
		'box on goal': pygame.image.load('BoxGoal.png'),
		'floor': pygame.image.load('Floor.png'),
		'grass': pygame.image.load('Grass.png'),
		'title': pygame.image.load('Title.png'),
		'solved': pygame.image.load('Solved.png')
	}

	Tile_mapping = {
		'#' : Image_dictionary['wall'],
		'o' : Image_dictionary['floor'],
		' ' : Image_dictionary['grass']
	}

	Player = Image_dictionary['player']
	startScreen()
	levels = read_Levels_File('sokobanLevels.txt')
	current_Level_Index = 0
	while True:
		result = run_Level(levels, current_Level_Index)
		if result in ('solved', 'next'):
			current_Level_Index += 1
			if current_Level_Index >= len(levels):
				current_Level_Index = 0
		elif result == 'back':
			current_Level_Index -= 1
			if current_Level_Index < 0:
				current_Level_Index = len(levels)-1
		elif result == 'reset':
			pass

def run_Level(levels, level_number):
    level_object = levels[level_number]
    map_object = build_map(level_object['map_object'], level_object['startState']['player'])
    game_state = copy.deepcopy(level_object['startState'])
    refresh_map = True
    level_surface = Basic_font.render('Level %s of %s' % (level_number + 1, len(levels)), 1, white)
    level_rectangle = level_surface.get_rect()
    level_rectangle.bottomleft = (20, 600 - 35)
    map_width = len(map_object) * Tile_width
    map_height = (len(map_object[0]) - 1) * Tile_height
    max_cam_x = abs(300 - int(map_height / 2)) + Tile_width
    max_cam_y = abs(400 - int(map_width / 2)) + Tile_height

    level_is_complete = False
    cam_offset_x = 0
    cam_offset_y = 0
    cam_Up = False
    cam_Down = False
    cam_Left = False
    cam_Right = False

    while True: 
        player_move_to = None
        key_pressed = False

        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()

            elif event.type == KEYDOWN:
                key_pressed = True
                if event.key == K_LEFT:
                    player_move_to = LEFT
                elif event.key == K_RIGHT:
                    player_move_to = RIGHT
                elif event.key == K_UP:
                    player_move_to = UP
                elif event.key == K_DOWN:
                    player_move_to = DOWN

                elif event.key == K_a:
                    cam_Left = True
                elif event.key == K_d:
                    cam_Right = True
                elif event.key == K_w:
                    cam_Up = True
                elif event.key == K_s:
                    cam_Down = True

                elif event.key == K_n:
                    return 'next'
                elif event.key == K_b:
                    return 'back'

                elif event.key == K_ESCAPE:
                    terminate()
                elif event.key == K_BACKSPACE:
                    return 'reset'

            elif event.type == KEYUP:
                if event.key == K_a:
                    cam_Left = False
                elif event.key == K_d:
                    cam_Right = False
                elif event.key == K_w:
                    cam_Up = False
                elif event.key == K_s:
                    cam_Down = False

        if player_move_to != None and not level_is_complete:
            moved = make_move(map_object, game_state, player_move_to)

            if moved:
                game_state['stepCounter'] += 1
                refresh_map = True

            if is_level_finished(level_object, game_state):
                level_is_complete = True
                if sound == 0:
                	tada = pygame.mixer.Sound('tada.wav')
                	tada.set_volume(0.5)
                key_pressed = False

        Display_surface.fill(blue)

        if refresh_map:
            map_surface = draw_map(map_object, game_state, level_object['goals'])
            refresh_map = False

        if cam_Up and cam_offset_y < max_cam_x:
            cam_offset_y += Cam_speed
        elif cam_Down and cam_offset_y > -max_cam_x:
            cam_offset_y -= Cam_speed
        if cam_Left and cam_offset_x < max_cam_y:
            cam_offset_x += Cam_speed
        elif cam_Right and cam_offset_x > -max_cam_y:
            cam_offset_x -= Cam_speed

        map_surface_rectangle = map_surface.get_rect()
        map_surface_rectangle.center = (400 + cam_offset_x, 300 + cam_offset_y)

        Display_surface.blit(map_surface, map_surface_rectangle)
        Display_surface.blit(level_surface, level_rectangle)
        step_surface = Basic_font.render('Steps: %s' % (game_state['stepCounter']), 1, white)
        step_rectangle = step_surface.get_rect()
        step_rectangle.bottomleft = (20, 590)
        Display_surface.blit(step_surface, step_rectangle)

        if level_is_complete:
            solved_rectangle = Image_dictionary['solved'].get_rect()
            solved_rectangle.center = ((400, 300))
            Display_surface.blit(Image_dictionary['solved'], solved_rectangle)

            if key_pressed:
                return 'solved'

        pygame.display.update()
        Clock.tick()


def is_level_finished(level_object, game_state):
    for goal in level_object['goals']:
        if goal not in game_state['boxs']:
            return False
    return True


def terminate():
    pygame.quit()
    sys.exit()

def is_wall(map_object, x, y):
    if x < 0 or x >= len(map_object) or y < 0 or y >= len(map_object[x]):
        return False 
    elif map_object[x][y] in ('#', 'x'):
        return True
    return False

def is_blocked(map_object, game_state, x, y):
    if is_wall(map_object, x, y):
        return True

    elif x < 0 or x >= len(map_object) or y < 0 or y >= len(map_object[x]):
        return True 
    elif (x, y) in game_state['boxs']:
        return True
    return False


def make_move(map_object, game_state, player_move_to):

    player_x, player_y = game_state['player']
    boxs = game_state['boxs']
    if player_move_to == UP:
        x_offset = 0
        y_offset = -1
    elif player_move_to == RIGHT:
        x_offset = 1
        y_offset = 0
    elif player_move_to == DOWN:
        x_offset = 0
        y_offset = 1
    elif player_move_to == LEFT:
        x_offset = -1
        y_offset = 0

    if is_wall(map_object, player_x + x_offset, player_y + y_offset):
        return False
    else:
        if (player_x + x_offset, player_y + y_offset) in boxs:
            if not is_blocked(map_object, game_state, player_x + (x_offset*2), player_y + (y_offset*2)):
                ind = boxs.index((player_x + x_offset, player_y + y_offset))
                boxs[ind] = (boxs[ind][0] + x_offset, boxs[ind][1] + y_offset)
            else:
                return False
        game_state['player'] = (player_x + x_offset, player_y + y_offset)
        return True


def build_map(map_object, startxy):

    startx, starty = startxy 
    map_object_copy = copy.deepcopy(map_object)

    for x in range(len(map_object_copy)):
        for y in range(len(map_object_copy[0])):
            if map_object_copy[x][y] in ('$', '.', '@', '+', '*'):
                map_object_copy[x][y] = ' '

    floodFill(map_object_copy, startx, starty, ' ', 'o')
    return map_object_copy

def floodFill(map_object, x, y, oldCharacter, newCharacter):

    if map_object[x][y] == oldCharacter:
        map_object[x][y] = newCharacter

    if x < len(map_object) - 1 and map_object[x+1][y] == oldCharacter:
        floodFill(map_object, x+1, y, oldCharacter, newCharacter) # call right
    if x > 0 and map_object[x-1][y] == oldCharacter:
        floodFill(map_object, x-1, y, oldCharacter, newCharacter) # call left
    if y < len(map_object[x]) - 1 and map_object[x][y+1] == oldCharacter:
        floodFill(map_object, x, y+1, oldCharacter, newCharacter) # call down
    if y > 0 and map_object[x][y-1] == oldCharacter:
        floodFill(map_object, x, y-1, oldCharacter, newCharacter) # call up

def draw_map(map_object, game_state, goals):
    map_surface_width = len(map_object) * Tile_width
    map_surface_height = (len(map_object[0])) * Tile_height
    map_surface = pygame.Surface((map_surface_width, map_surface_height))
    map_surface.fill(blue)

    for x in range(len(map_object)):
        for y in range(len(map_object[x])):
            space_rectangle = pygame.Rect((x * Tile_width,y* Tile_height, Tile_width, Tile_height))
            if map_object[x][y] in Tile_mapping:
                base_tile = Tile_mapping[map_object[x][y]]

            map_surface.blit(base_tile, space_rectangle)

            if (x, y) in game_state['boxs']:
                if (x, y) in goals:
                    map_surface.blit(Image_dictionary['box on goal'], space_rectangle)
                map_surface.blit(Image_dictionary['box'], space_rectangle)
            elif (x, y) in goals:
                map_surface.blit(Image_dictionary['goal'], space_rectangle)
            if (x, y) == game_state['player']:
                map_surface.blit(Player, space_rectangle)

    return map_surface


def read_Levels_File(filename):
    assert os.path.exists(filename), 'Cannot find the level file: %s' % (filename)
    map_file = open(filename, 'r')
    content = map_file.readlines() + ['\r\n']
    map_file.close()

    levels = [] 
    level_number = 0
    map_text_lines = []
    map_object = []
    for line_number in range(len(content)):
        line = content[line_number].rstrip('\r\n')

        if ';' in line:
            line = line[:line.find(';')]

        if line != '':
            map_text_lines.append(line)
        elif line == '' and len(map_text_lines) > 0:
            max_width = -1
            for i in range(len(map_text_lines)):
                if len(map_text_lines[i]) > max_width:
                    max_width = len(map_text_lines[i])
            for i in range(len(map_text_lines)):
                map_text_lines[i] += ' ' * (max_width - len(map_text_lines[i]))

            for x in range(len(map_text_lines[0])):
                map_object.append([])
            for y in range(len(map_text_lines)):
                for x in range(max_width):
                    map_object[x].append(map_text_lines[y][x])

            startx = None
            starty = None
            goals = [] 
            boxs = []
            for x in range(max_width):
                for y in range(len(map_object[x])):
                    if map_object[x][y] in ('@', '+'):
                        startx = x
                        starty = y
                    if map_object[x][y] in ('.', '+', '*'):
                        goals.append((x, y))
                    if map_object[x][y] in ('$', '*'):
                        boxs.append((x, y))

            assert startx != None and starty != None, 'Level %s (around line %s) in %s is missing a "@" or "+" to mark the start point.' % (levelNum+1, lineNum, filename)
            assert len(goals) > 0, 'Level %s (around line %s) in %s must have at least one goal.' % (levelNum+1, lineNum, filename)
            assert len(boxs) >= len(goals), 'Level %s (around line %s) in %s is impossible to solve. It has %s goals but only %s boxs.' % (levelNum+1, lineNum, filename, len(goals), len(boxs))

            # Create level object and starting game state object.
            game_state = {'player': (startx, starty),
                            'stepCounter': 0,
                            'boxs': boxs}
            level_object = {'width': max_width,
                        'height': len(map_object),
                        'map_object': map_object,
                        'goals': goals,
                        'startState': game_state}

            levels.append(level_object)

            # Reset the variables for reading the next map.
            map_text_lines = []
            map_object = []
            game_state = {}
            level_number += 1
    return levels


def startScreen():
    titleRect = Image_dictionary['title'].get_rect() 
    Display_surface.blit(Image_dictionary['title'], titleRect)
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    terminate()
                return

        pygame.display.update()
        Clock.tick()

if __name__ == '__main__':
    main()